# Insurance Card website project for SWP391 subject

## Home screen
![image.png](./screens/image.png)

## Login screen
![login.png](./screens/login.png)

## Register screen
![register.png](./screens/register.png)

## Reset password screen
![forgotPass.png](./screens/forgotPass.png)