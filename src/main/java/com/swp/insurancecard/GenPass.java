package com.swp.insurancecard;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class GenPass {
    public static void main(String[] args){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String raw = "12345678";
        System.out.println(encoder.encode(raw));
    }
}
